# Starscape

## Overview
Starscape is an immersive MR experience that inspires aspiring musicians to engage with and understand musical concepts. It does this by allowing users to create visual, shareable galaxies, thereby fostering connection, confidence, and creativity.

## Setup
- Unity installed (version 2022.3.18f or later)
- Meta XR Plugin installed in your Unity project

### Hardware Required
- Meta Quest 3 device

### Software Dependencies

Ensure you have the following dependencies installed and configured before setting up and running the project:

-Unity: Version 2022.3.18f or later. Download and install from the Unity website.

-Meta XR Plugin: Required for Meta Quest 3 integration. Follow the official Meta XR Plugin installation guide.

## How to run
Players begin in free play mode, learning to use their bodies to create stars in their virtual environment.

1. Open the project with unity version 2022.3.18f or later. 
2. Open the scene 'Starscape_Main', connect the Meta Quest 3 laptop and enter the play mode for the quest link mode.
3. Interact with the MR passthrough environment and press the click here button.
4. Follow the instructions on the screen and experience the visual musical world around you.

## Demo
Youtube link : https://www.youtube.com/watch?v=tVHNxJbyCts
