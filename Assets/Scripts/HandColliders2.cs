using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandColliders2 : MonoBehaviour
{
    public GameObject UIbutton;
    //public GameObject UIlogo;
    public GameObject HandWaveGesture;
  // public AudioSource Click;
    void OnTriggerEnter(Collider collision)
    {
        //Check for a match with the specific tag on any GameObject that collides with your GameObject
        if (collision.gameObject.tag == "Hands")
        {
            //If the GameObject has the same tag as specified, output this message in the console
            UIbutton.SetActive(false);
            HandWaveGesture.SetActive(true);
            //Click.Play();
            //UIlogo.SetActive(false);
            //StartCoroutine(HandGestureStart());
        }
    }

    /*IEnumerator HandGestureStart()
    {
        yield return new WaitForSeconds(3.0f);
        
        Debug.Log("it works");
    }*/
}
