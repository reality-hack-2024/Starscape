using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandColliders : MonoBehaviour
{
    public GameObject HandWaveGesture;
    public GameObject GestureCollider;
    public GameObject UIgesture;
    void OnTriggerEnter(Collider collision)
    {
        //Check for a match with the specific tag on any GameObject that collides with your GameObject
        if (collision.gameObject.tag == "Hands")
        {
            //If the GameObject has the same tag as specified, output this message in the console
            GestureCollider.SetActive(true);
            StartCoroutine(HandGestureStart());
            Debug.Log("it works");
        }
    }
    IEnumerator HandGestureStart()
    {
        yield return new WaitForSeconds(12.0f);
        HandWaveGesture.SetActive(true);
        GestureCollider.SetActive(false);
        UIgesture.SetActive(false);
        Debug.Log("it works");
    }
}
