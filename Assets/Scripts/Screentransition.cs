using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screentransition : MonoBehaviour
{
    public GameObject UIlogo;
    public GameObject BGeffex;
    public GameObject UIinstructions;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(HandGestureStart()); 
    }

   
    IEnumerator HandGestureStart()
    {
        yield return new WaitForSeconds(30.0f);
        UIinstructions.SetActive(false);
        UIlogo.SetActive(true);
        BGeffex.SetActive(true);
        Debug.Log("it works");
    }
}
